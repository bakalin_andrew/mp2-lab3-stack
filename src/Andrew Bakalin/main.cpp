// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// Copyright (c) Гергель В.П. 28.07.2000
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - тестирование стека

#include <iostream>
#include "tstack.h"

using namespace std;

void main()
{
	TStack<int> st(10);

	try 
	{
		for (int i = 0; i < 10; i++)
			st.Put(i);
		cout << "Our test stack is" << endl;
		for (int i = 0; i <= 10; i++) // сделана ошибка, вместо < написали <=
			cout << st.Get() << endl;
	}
	catch (int a)
	{
		cout << "Error code:" << st.GetRetCode() << endl; // ошибка поймана!
	}
}
