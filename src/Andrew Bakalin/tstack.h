#ifndef _TSTACK_
#define _TSTACK_

#include <iostream>
#include "tdataroot.h"

#define MemAddedIncorrect -105 //������������ �������� MemAdded
#define DefMemAdded 25

template <class T>
class TStack : public TDataRoot<T>
{
protected:
	int top, memAdded;
	void addMem();

public:
	TStack(int Size = DefMemSize, int MemAdded = DefMemAdded);
	TStack(const TStack<T>&);
	void Put(const T&) override;
	T Get() override;
	void Print() override;
};

template <class T>
void TStack<T>::addMem()
{
	T* temp;

	temp = new T[MemSize + memAdded];

	for (int i = 0; i < DataCount; i++)
		temp[i] = pMem[i];
	delete[] pMem;
	pMem = temp;

	MemSize += memAdded;
}

template <class T>
TStack<T>::TStack(int Size, int MemAdded) : TDataRoot<T>(Size)
{
	top = -1;

	if (MemAdded > 0)
		memAdded = MemAdded;
	else
		throw SetRetCode(MemAddedIncorrect);
}

template <class T>
TStack<T>::TStack(const TStack<T> &st) : TDataRoot<T>(st)
{
	top = st.top;
	memAdded = st.memAdded;
}

template <class T>
void TStack<T>::Put(const T &data)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else if (IsFull())
	{
		if (MemType == MEM_RENTER)
			throw SetRetCode(DataFull);
		else
			addMem();
	}

	pMem[++top] = data;
	DataCount++;
}

template <class T>
T TStack<T>::Get()
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else if (IsEmpty())
		throw SetRetCode(DataEmpty);
	else
	{
		DataCount--;
		return pMem[top--];
	}
}

template <class T>
void TStack<T>::Print()
{
	for (int i = top; i > -1; i--)
		std::cout << pMem[i] << std::endl;
}

#endif
