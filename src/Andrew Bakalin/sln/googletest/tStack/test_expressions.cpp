#include "expressions.h"
#include <gtest/gtest.h>

TEST(IsCorrect, no_errors_if_expression_is_correct)
{
	string expr = "((5+3)*7)/(34+5)*(78-45)";

	EXPECT_EQ(0, IsCorrect(expr));
}

TEST(IsCorrect, show_correct_number_of_errors)
{
	string expr1 = "((5+3*7)/)((34+5)*(78-45)";

	string expr2 = "((((";

	EXPECT_EQ(1, IsCorrect(expr1) == 1 && IsCorrect(expr2) == 4);
}

TEST(CalcOfExpr, expression_calculates_correctly)
{
	double res = (1 + 2) / (3 + 4 * 6.7) - 5.3 * 4.4;
	string expr = "(1+2)/(3+4*6.7)-5.3*4.4";

	EXPECT_EQ(res, CalcOfExpr(expr));
}