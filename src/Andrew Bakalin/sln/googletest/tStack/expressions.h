#ifndef _EXPRESSIONS_
#define _EXPRESSIONS_

#include <string>
using namespace std;

double CalcOfExpr(string expr); // �������, ������������ ���������������� ���������� ��������� �� ��� ����������� �����
int IsCorrect(string expr); // ������� �������� �� ������������ ����������� ������
string InfixToPolish(string expr); //�������, ����������� ��������� �� ��������� ����� � �����������
int GetOperationPrio(char c); // �������, ������������ ��������� ��������

#endif
